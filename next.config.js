/** @type {import('next').NextConfig} */
module.exports = {
    reactStrictMode: true,
    images: {
        domains: ['upload.wikimedia.org'],
    },
    env: {
        API_URL: 'http://localhost:3000/api',
    },
}
