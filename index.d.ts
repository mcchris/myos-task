interface Product {
    id: string
    title: string
    description: string
    price: string
    image_url: string
    image_width: number
    image_height: number
}

interface Item {
    product_id: string
    quantity: number
}

interface ProductItem {
    product: Product
    quantity: number
}

interface Order {
    id: string
    cart: Items[]
    email: string
    ordered_at: string
}

interface APIError {
    message: string
}
