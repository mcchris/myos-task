import '../styles/globals.css'
import type { AppProps } from 'next/app'
import Head from 'next/head'

function MyosTask({ Component, pageProps }: AppProps) {
    return (
        <>
            <Head>
                <title>Myos Task</title>
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1.0"
                />
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <Component {...pageProps} />
        </>
    )
}

export default MyosTask
