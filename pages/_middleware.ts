import type { NextRequest } from 'next/server'
import { NextResponse } from 'next/server'
import { nanoid } from 'nanoid'

export function middleware(req: NextRequest) {
    const res = NextResponse.next()
    if ('cart_id' in req.cookies) {
        return res
    }
    const expires = new Date()
    expires.setFullYear(expires.getFullYear() + 1)
    const id = nanoid()
    res.cookie('cart_id', id, {
        sameSite: 'strict',
        httpOnly: true,
        expires,
    })
    return res
}
