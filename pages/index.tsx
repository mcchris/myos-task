import type { NextPage, GetServerSideProps } from 'next'
import { useState } from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { ProductCard } from '../components/ProductCard'
import { Header } from '../components/Header'
import getCart from '../lib/getCart'
import getProducts from '../lib/getProducts'

function SearchInput() {
    const router = useRouter()
    const [search, setSearch] = useState(
        typeof router.query.search === 'string' ? router.query.search : ''
    )
    return (
        <form
            onSubmit={(event) => {
                event.preventDefault()
                router.push('/?q=' + search)
            }}
        >
            <input
                type="text"
                className="text-box"
                placeholder="Search 🔍"
                value={typeof search === 'string' ? search : ''}
                onChange={(event) => setSearch(event.target.value)}
            />
        </form>
    )
}

interface Props {
    products: Product[]
    cart: Item[]
}
const Home: NextPage<Props> = ({ products, ...props }) => {
    const [cart, setCart] = useState(props.cart)
    const totalCount = cart.reduce((total, item) => total + item.quantity, 0)

    async function handleAddToCard({
        id,
        quantity,
    }: {
        id: string
        quantity: number
    }) {
        const newCart = [...cart]
        const index = newCart.findIndex((item) => item.product_id === id)
        if (index !== -1) {
            const item = { ...newCart[index] }
            item.quantity += quantity
            newCart[index] = item
        } else {
            newCart.push({ product_id: id, quantity })
        }
        const res = await fetch(process.env.API_URL + '/cart', {
            method: 'POST',
            body: JSON.stringify(newCart),
        })
        setCart(await res.json())
    }

    return (
        <>
            <Header>
                <SearchInput />
                <Link href="/cart">
                    <a className="button bg-white border-blue-500">
                        🛒 Cart{' '}
                        <span className="text-orange-600">{totalCount}</span>
                    </a>
                </Link>
            </Header>
            <main className="container mx-auto">
                <div className="flex flex-col gap-4 p-4 max-w-screen-lg">
                    {products.length ? (
                        products.map((product) => (
                            <ProductCard
                                key={product.id}
                                product={product}
                                onAddToCart={handleAddToCard}
                            />
                        ))
                    ) : (
                        <p>No product found.</p>
                    )}
                </div>
            </main>
        </>
    )
}

export const getServerSideProps: GetServerSideProps = async (context) => {
    const cartId = context.req.cookies.cart_id
    let search = context.query.q
    search = typeof search === 'string' ? search : undefined
    const [cart, products] = await Promise.all([
        cartId ? getCart(cartId) : [],
        getProducts(search),
    ])
    return {
        props: {
            cart,
            products,
        },
    }
}

export default Home
