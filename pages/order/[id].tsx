import type { NextPage, GetServerSideProps } from 'next'
import Link from 'next/link'
import getOrder from '../../lib/getOrder'

interface Props {
    order: Order
}
const Order: NextPage<Props> = ({ order }) => {
    return (
        <main className="container mx-auto">
            <div className="flex flex-col gap-4 p-4 max-w-screen-lg">
                <div className="box text-center">
                    <h1 className="text-xl font-bold">Order Completed!</h1>
                    <p>Order ID: {order.id}</p>
                    <p className="mt-4">
                    <Link href="/">
                        <a className="button bg-white border-blue-500">
                            🏠 Home
                        </a>
                    </Link>
                    </p>
                </div>
            </div>
        </main>
    )
}

export const getServerSideProps: GetServerSideProps = async (context) => {
    const id = context.query.id
    if (typeof id !== 'string') {
        return {
            notFound: true,
        }
    }
    const order = await getOrder(id)
    if (!order) {
        return {
            notFound: true,
        }
    }

    return {
        props: {
            order,
        },
    }
}

export default Order
