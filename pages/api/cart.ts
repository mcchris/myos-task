import type { NextApiRequest, NextApiResponse } from 'next'
import { db } from '../../lib/db'

/**
 * Store/update a cart. The cart is stored as an incomplete order in the orders table.
 */
export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<Item[]>
) {
    if (req.method !== 'POST') {
        res.status(405)
    }
    return new Promise<void>((resolve, reject) => {
        db.serialize(() => {
            db.run(
                'INSERT INTO orders(id, cart) VALUES(?, ?) ON CONFLICT(id) DO UPDATE SET cart = excluded.cart',
                [req.cookies.cart_id, req.body]
            )
            db.get(
                'SELECT * FROM orders WHERE id = ?',
                req.cookies.cart_id,
                (error, row) => {
                    if (error) return reject(error)
                    res.status(200).json(row.cart)
                    resolve()
                }
            )
        })
    })
}
