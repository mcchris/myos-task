import type { NextApiRequest, NextApiResponse } from 'next'
import { db } from '../../lib/db'
import getOrder from '../../lib/getOrder'

/**
 * Handle orders. Convert a cart to order. When an order is completed, expire the
 * cart_id token, thus creating a new empty cart.
 */
export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<Order | APIError>
) {
    if (req.method !== 'POST') {
        res.status(405)
    }

    const order = await getOrder(req.cookies.cart_id)

    // Order already exists
    if (order) {
        res.setHeader(
            'Set-Cookie',
            'cart_id=1; Path=/; Max-Age=-1; SameSite=Strict; HttpOnly'
        )
        res.status(400).json({
            message: 'Invalid order',
        })
        return res
    }

    return new Promise<void>((resolve, reject) => {
        db.serialize(() => {
            db.run('UPDATE orders SET email = ?, ordered_at = ?', [
                req.body,
                new Date().toISOString(),
            ])
            db.get(
                'SELECT * FROM orders WHERE id = ?',
                req.cookies.cart_id,
                (error, row) => {
                    if (error) return reject(error)
                    res.setHeader(
                        'Set-Cookie',
                        'cart_id=1; Path=/; Max-Age=-1; SameSite=Strict; HttpOnly'
                    )
                    res.status(200).json({ ...row, cart: JSON.parse(row.cart) })
                    resolve()
                }
            )
        })
    })
}
