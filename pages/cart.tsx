import { useState } from 'react'
import type { NextPage, GetServerSideProps } from 'next'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { Header } from '../components/Header'
import { ItemCard } from '../components/ItemCard'
import getCart from '../lib/getCart'
import getTotal from '../lib/getTotal'
import getProducts from '../lib/getProducts'

function HomeButton() {
    return (
        <Link href="/">
            <a className="button bg-white border-blue-500">🏠 Home</a>
        </Link>
    )
}

interface Props {
    products: Product[]
    cart: Item[]
}
const Cart: NextPage<Props> = ({ products, ...props }) => {
    const router = useRouter()
    const [cart, setCart] = useState(props.cart)
    const [email, setEmail] = useState('')
    const [submitting, setSubmitting] = useState(false)
    const [error, setError] = useState('')
    const items = cart.map((item) => {
        const product = products.find(
            (product) => product.id === item.product_id
        )
        if (!product) {
            throw new Error('Missing product.')
        }
        return {
            ...item,
            product,
        }
    })

    const total = getTotal(items)

    async function handleQuantityChange({
        id,
        quantity,
    }: {
        id: string
        quantity: number
    }) {
        const newCart = [...cart]
        const index = newCart.findIndex((item) => item.product_id === id)
        if (index !== -1) {
            const item = { ...newCart[index] }
            item.quantity = quantity
            newCart[index] = item
        } else {
            newCart.push({ product_id: id, quantity })
        }
        const res = await fetch(process.env.API_URL + '/cart', {
            method: 'POST',
            body: JSON.stringify(newCart),
        })
        setCart(await res.json())
    }

    async function handleItemDelete(id: string) {
        const newCart = [...cart]
        const index = newCart.findIndex((item) => item.product_id === id)
        if (index !== -1) {
            newCart.splice(index, 1)
        } else {
            throw new Error('Item not found')
        }
        const res = await fetch(process.env.API_URL + '/cart', {
            method: 'POST',
            body: JSON.stringify(newCart),
        })
        setCart(await res.json())
    }

    async function handleCompleteOrder() {
        setSubmitting(true)
        const res = await fetch(process.env.API_URL + '/order', {
            method: 'POST',
            body: email,
        })
        if (res.status !== 200) {
            setError('There was a problems processing your order')
        } else {
            const order = await res.json()
            router.push(`/order/${order.id}`)
        }
        setSubmitting(false)
    }

    return (
        <>
            <Header>
                <HomeButton />
            </Header>
            <main className="container mx-auto">
                <h1 className="text-xl font-bold pt-4 px-4">Cart</h1>
                {cart.length ? (
                    <div className="flex flex-wrap">
                        <div className="flex flex-col gap-4 p-4 max-w-screen-md">
                            {items.map((item, index) => (
                                <ItemCard
                                    key={index}
                                    product={item.product}
                                    quantity={item.quantity}
                                    onQuantityChange={handleQuantityChange}
                                    onDelete={handleItemDelete}
                                />
                            ))}
                        </div>
                        <div className="p-4">
                            <form
                                className="box flex flex-col"
                                onSubmit={(event) => {
                                    event.preventDefault()
                                    handleCompleteOrder()
                                }}
                            >
                                <h2 className="text-xl font-medium">
                                    Checkout
                                </h2>

                                <p className="my-2">
                                    Total:{' '}
                                    <span className="font-bold">${total}</span>
                                </p>

                                <label htmlFor="email" className="text-xs mt-2">
                                    Email address
                                </label>
                                <input
                                    name="email"
                                    type="email"
                                    className="text-box"
                                    value={email}
                                    placeholder="hello@example.com"
                                    required
                                    onChange={(event) => {
                                        setEmail(event.target.value)
                                    }}
                                />

                                <button
                                    type="submit"
                                    className="button mt-2 text-white bg-blue-600"
                                    disabled={submitting}
                                >
                                    Complete Order
                                </button>
                                {!!error && (
                                    <p className="text-red-500">{error}</p>
                                )}
                            </form>
                        </div>
                    </div>
                ) : (
                    <p className="px-4">Cart empty.</p>
                )}
            </main>
        </>
    )
}

export const getServerSideProps: GetServerSideProps = async (context) => {
    const cartId = context.req.cookies.cart_id
    const res = await Promise.all([
        cartId ? getCart(cartId) : new Array<Item>(),
        getProducts(),
    ])
    return {
        props: {
            cart: res[0],
            products: res[1],
        },
    }
}

export default Cart
