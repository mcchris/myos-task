/*
 * Compute the total given a cart.
 */
export default function getTotal(cart: ProductItem[]): number {
    let total = cart.reduce((total, item) => {
        const price = Number(item.product.price)
        if (Number.isNaN(price)) {
            throw new Error('Invalid price')
        }
        return total + price * item.quantity
    }, 0)
    return Math.round(total * 100) / 100
}
