import sqlite from 'sqlite3'
import products from '../seeds/products.json'

const db = new sqlite.Database('db.sqlite')

// Create database tables and populate when necessary
db.serialize(() => {
    db.run(`CREATE TABLE IF NOT EXISTS orders(
    id TEXT PRIMARY KEY,
    cart TEXT,
    email TEXT,
    ordered_at TEXT
);`)
    db.run(`CREATE TABLE IF NOT EXISTS products(
    id TEXT PRIMARY KEY,
    price REAL NOT NULL,
    image_url TEXT NOT NULL,
    image_width INTEGER NOT NULL,
    image_height INTEGER NOT NULL
);`)

    // Store title and description in a virtual table to enable full-text search
    db.run(`CREATE VIRTUAL TABLE IF NOT EXISTS product_texts
USING FTS5(
    id,
    title,
    description
);`)

    db.get('SELECT * FROM products', (error, row) => {
        if (error) throw error
        if (row) return

        const insertProduct = db.prepare(
            'INSERT INTO products VALUES ($id, $price, $image_url, $image_width, $image_height)'
        )
        const insertProductText = db.prepare(
            'INSERT INTO product_texts VALUES ($id, $title, $description)'
        )
        products.forEach((product) => {
            insertProduct.run({
                $id: product.id,
                $price: product.price,
                $image_url: product.image_url,
                $image_width: product.image_width,
                $image_height: product.image_height,
            })
            insertProductText.run({
                $id: product.id,
                $title: product.title,
                $description: product.description,
            })
        })
    })
})

export { db }
