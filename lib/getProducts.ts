import { db } from './db'

/*
 * Get all products. Optional search string to match product title and
 * description using full-text search.
 */
export default async function getProducts(search?: string): Promise<Product[]> {
    return new Promise((resolve, reject) => {
        let query = `SELECT p.*, pt.title, pt.description FROM product_texts AS pt
LEFT JOIN products AS p ON p.id = pt.id`
        if (search) {
            query = `${query} WHERE product_texts MATCH ? ORDER BY rank`
            db.all(query, `"${search}"`, (error, rows) => {
                if (error) return reject(error)
                resolve(rows)
            })
        } else {
            db.all(query, (error, rows) => {
                if (error) return reject(error)
                resolve(rows)
            })
        }
    })
}
