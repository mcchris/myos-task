import { db } from './db'

/*
 * Get an order's cart.
 */
export default async function getCart(id: string): Promise<Item[]> {
    return new Promise((resolve, reject) => {
        db.get('SELECT * FROM orders WHERE id = ?', id, (error, row) => {
            if (error) return reject(error)
            const cart = row?.cart ? JSON.parse(row.cart) : []
            resolve(cart)
        })
    })
}
