import { db } from './db'

/**
 * Get complete orders.
 */
export default async function getOrder(id: string): Promise<Order | undefined> {
    return new Promise((resolve, reject) => {
        db.get(
            'SELECT * FROM orders WHERE id = ? AND ordered_at IS NOT NULL',
            id,
            (error, row) => {
                if (error) return reject(error)
                if (!row) return resolve(undefined)
                resolve({
                    ...row,
                    cart: JSON.parse(row.cart),
                })
            }
        )
    })
}
