## Getting Started

Install dependencies:

```bash
npm install
```

First, run the development server:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the
result.

## Architecture

This is a simple Next.js app with the following pages:

-   `/` - lists the products and displays the total cart items. This is the page
    where you can add products to the cart. You can also search products using
    keywords from this page.
-   `/cart` - lists all the cart items and displays the checkout tab. You can
    modify the quantity of each item or remove it from the cart.
-   `/order/[id]` - displays an "Order completed" message.

### Notes

-   Minimal setup:
    -   Next.js as the base framework
    -   Tailwind for quick styling
    -   SQLite for data storage
-   Carts are identified by a random ID stored in a HttpOnly cookie. Once the
    order is completed, the cookie is expired and a new cart/cookie is created.
-   Carts are just orders that are not completed yet. To complete an order, just
    populate the `email` and `ordered_at` columns in the `orders` table.
-   No extra setup needed. When running the app, it automatically creates an
    SQLite db file, creates the tables, and populates the products when
    necessary.
-   Search is powered by SQLite full-text search.
