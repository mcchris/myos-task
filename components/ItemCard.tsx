import Image from 'next/image'

interface Props {
    product: Product
    quantity: number
    onQuantityChange(params: { id: string; quantity: number }): void
    onDelete(id: string): void
}
export function ItemCard({ product, quantity, ...props }: Props) {
    return (
        <div className="box flex flex-col md:flex-row gap-x-4">
            <div className="w-1/4">
                <Image
                    src={product.image_url}
                    alt="Book cover"
                    width={product.image_width}
                    height={product.image_height}
                />
            </div>
            <div className="flex flex-col w-3/4">
                <div className="flex justify-between">
                    <p className="text-lg font-medium self-end">
                        {product.title}
                    </p>
                    <button
                        className="button"
                        onClick={() => {
                            if (
                                window.confirm(
                                    `Are you sure you want to remove ${product.title} from your cart?`
                                )
                            ) {
                                props.onDelete(product.id)
                            }
                        }}
                    >
                        🗑
                    </button>
                </div>
                <p className="text-md font-medium">${product.price}</p>
                <input
                    type="number"
                    className="text-box mt-2 w-20"
                    value={quantity}
                    onChange={(event) => {
                        let value = parseInt(event.target.value ?? '0', 10)
                        value = Math.abs(Number.isNaN(value) ? 0 : value)
                        props.onQuantityChange({
                            id: product.id,
                            quantity: value,
                        })
                    }}
                />
            </div>
        </div>
    )
}
