import { useState } from 'react'
import Image from 'next/image'

interface Props {
    product: Product
    onAddToCart(params: { id: string; quantity: number }): void
}
export function ProductCard({ product, onAddToCart }: Props) {
    const [quantity, setQuantity] = useState(1)

    return (
        <div className="box flex flex-col md:flex-row gap-x-4">
            <div className="md:w-1/4">
                <Image
                    src={product.image_url}
                    alt="Book cover"
                    width={product.image_width}
                    height={product.image_height}
                />
            </div>
            <div className="flex flex-col md:w-3/4">
                <p className="text-xl font-medium">{product.title}</p>
                <p className="text-md font-medium">${product.price}</p>
                <p className="flex">{product.description}</p>
                <div className="flex gap-2 mt-2">
                    <input
                        type="number"
                        className="text-box w-20"
                        value={quantity}
                        onChange={(event) => {
                            let value = parseInt(event.target.value ?? '0', 10)
                            value = Math.abs(Number.isNaN(value) ? 0 : value)
                            setQuantity(value)
                        }}
                    />
                    <button
                        className="button bg-blue-300 border-blue-400"
                        disabled={quantity < 1}
                        onClick={() => {
                            onAddToCart({
                                id: product.id,
                                quantity,
                            })
                            setQuantity(1)
                        }}
                    >
                        Add to cart
                    </button>
                </div>
            </div>
        </div>
    )
}
