import Link from 'next/link'
import { ReactNode } from 'react'

interface Props {
    children?: ReactNode
}
export function Header({ children }: Props) {
    return (
        <header className="flex justify-between items-center bg-slate-300 p-2">
            <h1 className="text-xl font-bold">
                <Link href="/">
                    <a>Myos Bookshop</a>
                </Link>
            </h1>
            <div className="flex gap-2">{children}</div>
        </header>
    )
}
